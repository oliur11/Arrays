<?php
 echo '<b>Example-1:</b><br>';
$salaries = array("mohammad" => 2000, "qadir" => 1000, "zara" => 500);
         
         echo "Salary of mohammad is ". $salaries['mohammad'] . "<br />";
         echo "Salary of qadir is ".  $salaries['qadir']. "<br />";
         echo "Salary of zara is ".  $salaries['zara']. "<br />";
         echo '<br>';
         
         echo '<b>Example-2:</b><br>';
         
         $salaries['mohammad'] = "high";
         $salaries['qadir'] = "medium";
         $salaries['zara'] = "low";
         
         echo "Salary of mohammad is ". $salaries['mohammad'] . "<br />";
         echo "Salary of qadir is ".  $salaries['qadir']. "<br />";
         echo "Salary of zara is ".  $salaries['zara']. "<br />";
         echo '<br>';
         
         echo '<b>Example-3:</b><br>';
         
         $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
          echo "Peter is " . $age['Peter'] . " years old.";
         echo '<br>';
         
         echo '<b>Example-4:</b><br>';
         
         
         $age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

              foreach($age as $x => $x_value) 
                  {
              echo "Key=" . $x . ", Value=" . $x_value;
              }
              echo "<br>";
         
         echo '<b>Example-5:</b><br >';
         
         
         $first_array = array("key1" => "the first element", "key2" => "the second element");

         $second_array = array(
                                     "key3" => "this is the first element of the second array",
                                     "key4" => "this is the second element of the second array",
                                    );
                                        echo $first_array['key1'];
                                        echo "<br>";
                                        echo $second_array['key3'];  
                                        echo "<br>";
                                       echo $first_array['key2'];
                                       echo "<br>";
                                        echo $second_array['key4'];
 ?>