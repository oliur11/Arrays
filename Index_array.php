<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo "<b>Example-1:</b><br>"; 
        $numbers = array( 1, 2, 3, 4, 5);
         
         foreach( $numbers as $value )
         {
            echo "Value is $value <br />";
         }
         
         /* Second method to create array. */
         $numbers[0] = "one";
         $numbers[1] = "two";
         $numbers[2] = "three";
         $numbers[3] = "four";
         $numbers[4] = "five";
         
         foreach( $numbers as $value )
         {
            echo "Value is $value <br />";
         }
         
         echo '<b>Example -2:</b><br>';
         
         
               $cars = array("Volvo", "BMW", "Toyota");
               echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
                echo " <br />";

         
         echo '<b>Example -3:</b><br>';
                
                 $array = array("foo", "bar", "hello", "world");
                 var_dump($array);
                 echo " <br />";
         echo '<b>Example -4:</b><br>';
         
         $cars = array("Volvo", "BMW", "Toyota");
           echo count($cars);
           echo " <br />";
         
         echo '<b>Example -5:</b><br>';
         
         $cars = array("Volvo", "BMW", "Toyota");
            $arrlength = count($cars);

               for($x = 0; $x < $arrlength; $x++)
               {
                   echo $cars[$x];
                   echo '<br>';
               }
         
         
         
        ?>
    </body>
</html>
